#include "../include/image.h"
#include "bmp.h"

static enum read_status to_image(FILE* file, struct bmp_header* header, struct image* new_image) {
    *new_image = create_image(header->biHeight, header->biWidth);
    fseek(file, header->bOffBits, SEEK_SET);

    for (uint32_t i = 0; i < header->biHeight; i++) {
        if (fread(&new_image->data[i * header->biWidth], sizeof(struct pixel), header->biWidth, file) != header->biWidth) {
            free_data(new_image);
            close(file);
            return READ_INVALID_PIXEL;
        }

        fseek(file, (int64_t)(4 - (header->biWidth * sizeof(struct pixel)) % 4) % 4, SEEK_CUR);
    }

    return READ_OK;
}

enum write_status to_bmp(FILE* file, struct bmp_header* header, struct image* image) {
    header->biWidth = image->width;
    header->biHeight = image->height;

    if (fwrite(header, sizeof(struct bmp_header), 1, file) != 1 || fseek(file, header->bOffBits, SEEK_SET) != 0) {
        close(file);
        free_data(image);
        return WRITE_ERROR;
    }

    int64_t padding = (int64_t)(4 - (image->width * sizeof(struct pixel)) % 4) % 4;

    for (uint32_t i = 0; i < image->height; i++) {
        if (fwrite(&image->data[i * image->width], sizeof(struct pixel), image->width, file) != image->width) {
            close(file);
            free_data(image);
            return WRITE_ERROR;
        }

        for (int64_t p = 0; p < padding; p++) {
            fputc(0, file);
        }
    }

    return WRITE_OK;
}

enum read_status read_bmp_header(FILE* file, struct bmp_header* header) {
    return fread(header, sizeof(struct bmp_header), 1, file) == 1 ? READ_OK : READ_INVALID_HEAD;
}

enum read_status from_bmp(FILE* file, struct bmp_header* header, struct image* source_image) {
    if (read_bmp_header(file, header) != READ_OK) return READ_INVALID_HEAD;

    if (header->bfType != 19778) {
        close(file);
        return READ_INVALID_TYPE;
    }

    if (header->biBitCount != 24) {
        close(file);
        return READ_INVALID_COLOR_DEPTH;
    }

    return to_image(file, header, source_image);
}
