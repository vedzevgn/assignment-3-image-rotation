#include "../include/image.h"
#include <string.h>

struct pixel get_pixel(struct image image, uint32_t x, uint32_t y) {
    return image.data[x * image.width + y];
}

void set_pixel(struct image* image, struct pixel pixel, uint32_t x, uint32_t y) {
    image->data[x * image->width + y].red = pixel.red;
    image->data[x * image->width + y].green = pixel.green;
    image->data[x * image->width + y].blue = pixel.blue;
}

static void rotate_90d(struct pixel pixel, struct image* new_image, uint32_t i, uint32_t j) {
    uint32_t x = new_image->height - 1 - j;
    uint32_t y = i;
    set_pixel(new_image, pixel, x, y);
}

static void rotate_180d(struct pixel pixel, struct image* new_image, uint32_t i, uint32_t j) {
    uint32_t x = new_image->height - 1 - i;
    uint32_t y = new_image->width - 1 - j;
    set_pixel(new_image, pixel, x, y);
}

static void rotate_270d(struct pixel pixel, struct image* new_image, uint32_t i, uint32_t j) {
    uint32_t x = j;
    uint32_t y = new_image->width - 1 - i;
    set_pixel(new_image, pixel, x, y);
}

static void pixels_foreach(struct image image, struct image* new_image, void (*rotate)(struct pixel pixel, struct image* new_image, uint32_t i, uint32_t j)) {
    for (uint32_t i = 0; i < image.height; i++) {
        for (uint32_t j = 0; j < image.width; j++) {
            rotate(get_pixel(image, i, j), new_image, i, j);
        }
    }

}

struct image rotate(struct image image, int angle) {
    angle = angle % 360;

    if (angle == 0) {
        struct image new_image = create_image(image.height, image.width);
        if (new_image.data != NULL) {
            memcpy(new_image.data, image.data, image.height * image.width * sizeof(struct pixel));
        }
        return new_image;
    }

    struct image new_image;
    if (angle % 180 == 0) {
        new_image = create_image(image.height, image.width);
    } else {
        new_image = create_image(image.width, image.height);
    }

    if(angle == 90) {
        pixels_foreach(image, &new_image, rotate_90d);
    } else if(angle == 180) {
        pixels_foreach(image, &new_image, rotate_180d);
    } else {
        pixels_foreach(image, &new_image, rotate_270d);
    }

    return new_image;
}
