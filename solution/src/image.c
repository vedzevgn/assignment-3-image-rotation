#include "../include/image.h"

struct image create_image(uint32_t height, uint32_t width) {
    struct image new_image = {.width = width, .height = height, .data = malloc(height * width * (sizeof (struct pixel)))};
    return new_image;
}

void free_data(struct image* image) {
    free(image->data);
}
