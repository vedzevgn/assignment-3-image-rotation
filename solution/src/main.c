#include "../include/rotate.h"
#include "../include/bmp.h"
#include "../include/image.h"

int main(int count, char* args[]) {
    int angle = arguments_valid(count, args);

    FILE* file = open(args[1], "rb");
    struct image source_image = {0};
    struct bmp_header header = {0};

    enum read_status read_status = from_bmp(file, &header, &source_image);
    check_errors(read_status, WRITE_OK, file);

    close(file);

    struct image output_image = rotate(source_image, angle);
    free_data(&source_image);

    FILE *new_file = open(args[2], "wb"); 
    enum write_status write_status = to_bmp(new_file, &header, &output_image);
    check_errors(READ_OK, write_status, new_file);
        
    close(new_file);
    free_data(&output_image);
    
    return 0;
}
