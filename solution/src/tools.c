#include "../include/tools.h"
#include <stdint.h>

const char* read_errors[5] = {
        "Ошибка чтения.",
        "Файлы BMP с глубиной цвета не 24 бит не поддерживаются.",
        "Ошибка чтенния заголовка файла.",
        "Неверный формат файла. Подерживаются только файлы BMP.",
        "Невозможно открыть файл, проверьте имя файла и права доступа."
};

const char* write_errors[2] = {
        "Ошибка записи в файл.",
        "Ошибка закрытия файла."
};

const char* user_errors[2] = {
        "Доступные варианты угла поворота: 0, ±90, ±180, ±270.",
        "Корректное использование: ./image-transformer имя_файла имя_выходного_файла угол"
};

uint8_t check_errors(enum read_status read_state, enum write_status write_state, FILE* file) {
    if (read_state == READ_OK && write_state == WRITE_OK) {
        return 1;
    }

    if(read_state != READ_OK){
        printf("%s", read_errors[read_state]);
        close(file);
        exit(1);
    }

    if(write_state != WRITE_OK){
        printf("%s", write_errors[write_state]);
        close(file);
        exit(1);
    }

    return 0;
}

uint8_t arguments_valid(uint8_t count, char* args[]) {
    if (count == 4) {
        char* endptr;
        long angle = strtol(args[3], &endptr, 10);

        if (angle < 0 && angle >= -270) {
            angle += 360;
        } else if (labs(angle) > 270 || labs(angle % 90) != 0) {
            fprintf(stderr, "%s", user_errors[0]);
            exit(1);
        }

        return angle;
    }

    fprintf(stderr, "%s", user_errors[1]);
    exit(1);
}


FILE* open(char* file_name, char* mode) {
    FILE* file = fopen(file_name, mode);
    if (file == NULL) {
        fprintf(stderr, "%s", read_errors[4]);
        exit(1);
    }

    return file;
}

void close(FILE* file) {
    if (fclose(file) != 0) {
        fprintf(stderr, "%s", write_errors[1]);
        exit(1);
    }
}
