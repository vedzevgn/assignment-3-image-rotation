#ifndef ASSIGNMENT3_ROTATE_H
#define ASSIGNMENT3_ROTATE_H

struct image rotate(struct image image, int angle);

#endif //ASSIGNMENT3_ROTATE_H
