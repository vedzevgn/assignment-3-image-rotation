#ifndef ASSIGNMENT3_IMAGE_H
#define ASSIGNMENT3_IMAGE_H

#include <stdint.h>
#include <stdlib.h>

struct pixel {
    uint8_t red, blue, green;
};

struct image {
    uint64_t width, height;
    struct pixel* data;
};

void free_data(struct image* image);
struct image create_image(uint32_t height, uint32_t width);

#endif //ASSIGNMENT3_IMAGE_H
