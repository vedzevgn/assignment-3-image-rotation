#ifndef ASSIGNMENT3_TOOLS_H
#define ASSIGNMENT3_TOOLS_H

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


enum read_status  {
    READ_OK = 0,
    READ_INVALID_PIXEL,
    READ_INVALID_COLOR_DEPTH,
    READ_INVALID_HEAD,
    READ_INVALID_TYPE,
};

enum write_status  {
    WRITE_OK = 0,
    WRITE_ERROR
};

uint8_t arguments_valid(uint8_t count, char* args[]);
uint8_t check_errors(enum read_status read_state, enum write_status write_state, FILE* file);
FILE* open(char* file_name, char* mode);
void close(FILE* file);

#endif // ASSIGNMENT3_TOOLS_H
